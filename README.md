# Installation

* Install pip3:
```shell
$ sudo apt install python3-pip
```
	
* Install flask:
```shell
$ pip3 install Flask
$ pip3 install requests
$ sudo apt install python3-flask
```
	
To run the server
```shell
$ FLASK_APP=<server>.py flask run --host 0.0.0.0 --port 3000
```

To use curl
```shell
$ curl -d 'name=Stephanie' https://<URL>/greet
```
or
```
$ curl https://<URL>/greet?name=Megan
```

