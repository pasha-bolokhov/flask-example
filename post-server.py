#!/usr/bin/env python3

from flask import Flask, request

count = 0

app = Flask(__name__)
@app.route('/greet', methods=['POST', 'GET'])
def result():
	global count
	count += 1
	if request.method == 'POST':
		print('Received a request from ' + request.form['name'])
	else:
		print('Received a request from ' + request.args.get('name'))
	return 'Received ' + str(count) + ' times!'

# Can either run from the command line or invoke app.run()
app.run(debug=True, host='0.0.0.0', port=3000)

